.. _build:

Build flow
==========

.. autoclass:: tsfpga.vivado_project.VivadoProject()
    :members:

    .. automethod:: __init__


.. autoclass:: tsfpga.constraint.Constraint()
    :members:

    .. automethod:: __init__